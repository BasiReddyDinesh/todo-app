import React from "react";
import "./App.css";
import Todo from "./components/Todo";

export default class App extends React.PureComponent {
  render() {
    return (
      <div data-test="app-component" className="App">
        <Todo />
      </div>
    );
  }
}
