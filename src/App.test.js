import React from "react";
import { shallow } from "enzyme";
import { findElementBtAttrs } from "./testUtils";
import App from "./App";

const setup = () => shallow(<App />);

it("renders App component without fail", () => {
  const wrapper = setup();
  const appComp = findElementBtAttrs(wrapper, "app-component");
  expect(appComp.exists()).toBeTruthy();
});
