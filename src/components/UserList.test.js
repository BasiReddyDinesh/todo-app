import React from "react";
import { shallow } from "enzyme";
import { findElementBtAttrs, checkProps } from "../testUtils";

import UserList from "./UserList";
const defaultProps = {
  user: [],
};
const setup = (props = { ...defaultProps }) => shallow(<UserList {...props} />);

describe("renders UserList component without fail", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("renders UserList component without fail", () => {
    const userListComp = findElementBtAttrs(wrapper, "userlist-component");
    expect(userListComp.exists()).toBeTruthy();
  });
});

describe("verify by passing userList", () => {
  it("should have length 0 when there are no user exist", () => {
    const wrapper = setup();
    const nodes = findElementBtAttrs(wrapper, "user-items");
    expect(nodes.length).toBe(0);
  });
  it("should have length 1 when there are 1 user exist", () => {
    const wrapper = setup({ user: [{ id: 1, userName: "Dinesh" }] });
    const nodes = findElementBtAttrs(wrapper, "user-items");
    expect(nodes.length).toBe(1);
  });
  it("should have length 2 when there are 3 user exist", () => {
    const wrapper = setup({
      user: [
        { id: 1, userName: "Dinesh" },
        { id: 2, userName: "Deepa" },
      ],
    });
    const nodes = findElementBtAttrs(wrapper, "user-items");
    expect(nodes.length).toBe(2);
  });
});

it("check the props", () => {
  checkProps(UserList, defaultProps);
});
