import PropTypes from "prop-types";
import React, { Component } from "react";
import Button from "../utils/Button";
import Input from "../utils/Input";

export default class UserList extends Component {
  static propTypes = {
    user: PropTypes.array.isRequired,
  };

  state = { userName: "" };

  editUser = (eve, id) => {
    const user = [...this.props.user];
    if (eve.target.innerHTML === "Edit") {
      user[id - 1].isEdit = true;
      this.props.updateList(user);
      this.setState({ userName: user[id - 1].userName });
      return;
    }
    user[id - 1].isEdit = false;
    user[id - 1].userName = this.state.userName;
    this.setState({ userName: "" });
    this.props.updateList(user);
  };

  deleteUser = (id) => {
    const user = this.props.user?.filter((user) => {
      return id !== user.id;
    });
    this.props.updateList(user);
  };

  editUserName = (eve) => {
    this.setState({ userName: eve.target.value });
  };

  render() {
    return (
      <div className="container" data-test="userlist-component">
        {this.props.user?.map(({ id, userName, isEdit }) => (
          <div className="card" key={id} data-test="user-items">
            {isEdit ? (
              <Input
                inputStyle="editInput"
                value={this.state.userName}
                type="text"
                id="edit-input-element"
                onChangeHandler={(eve) => this.editUserName(eve)}
                placeHolder={""}
              />
            ) : (
              <div className="item" data-test={`user-account-${id}`}>
                {userName}
              </div>
            )}
            <div className="item">
              <Button
                id={id}
                name={isEdit ? "Update" : "Edit"}
                clickHandler={(eve) => this.editUser(eve, id)}
                buttonStyle={isEdit ? "updateButton" : "editButton"}
              />
              <Button
                id={id}
                name="Delete"
                clickHandler={() => this.deleteUser(id)}
                buttonStyle={"deleteButton"}
              />
            </div>
          </div>
        ))}
      </div>
    );
  }
}
