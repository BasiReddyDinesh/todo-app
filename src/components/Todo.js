import PropTypes from "prop-types";
import React from "react";

import UserList from "./UserList";
import Input from "../utils/Input";
import Button from "../utils/Button";

export default class Todo extends React.PureComponent {
  //   static propTypes = { second: third };
  state = { value: "", user: [] };
  onChangeHandler = (eve) => {
    this.setState({ ...this.state, value: eve.target.value });
  };
  updateList = (user) => {
    this.setState({ ...this.state, user: [...user] });
  };
  clickHandler = () => {
    if (this.state.value.trim() === "")
      return this.setState({ ...this.state, value: "" });
    this.setState({
      ...this.state,
      user: [
        ...this.state.user,
        {
          id: this.state.user.length + 1,
          userName: this.state.value,
          isEdit: false,
        },
      ],
      value: "",
    });
  };
  render() {
    return (
      <div data-test="todo-component">
        <Input
          inputStyle="addInput"
          type="text"
          id="add-input-element"
          value={this.state.value}
          onChangeHandler={(eve) => this.onChangeHandler(eve)}
          placeHolder="Enter text"
        />
        <Button
          id={1}
          buttonStyle="addButton"
          name="Add"
          clickHandler={this.clickHandler}
        />
        <UserList user={this.state.user} updateList={this.updateList} />
      </div>
    );
  }
}
