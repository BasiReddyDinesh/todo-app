import { shallow } from "enzyme";
import { findElementBtAttrs } from "../testUtils";

import Todo from "./Todo";

const setup = () => shallow(<Todo />);

it("renders Todo component without fail", () => {
  const wrapper = setup();
  const todoComp = findElementBtAttrs(wrapper, "todo-component");
  expect(todoComp.exists()).toBeTruthy();
});
