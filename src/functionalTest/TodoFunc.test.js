import React from "react";
import { mount } from "enzyme";
import { findElementBtAttrs } from "../testUtils";

import App from "../App";

const setup = () => mount(<App />);

describe("renders App and its child components without fail", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("renders App component without fail", () => {
    const appComp = findElementBtAttrs(wrapper, "app-component");
    expect(appComp.exists()).toBeTruthy();
  });
  it("renders todo component without fail", () => {
    const todoComp = findElementBtAttrs(wrapper, "todo-component");
    expect(todoComp.exists()).toBeTruthy();
  });
  it("renders input component without fail", () => {
    const inputComp = findElementBtAttrs(wrapper, "input-component");
    expect(inputComp.exists()).toBeTruthy();
  });
  it("renders button component without fail", () => {
    const buttonComp = findElementBtAttrs(wrapper, "button-component");
    expect(buttonComp.exists()).toBeTruthy();
  });
});

describe("add names into todo list and verify", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("initially the user list must be 0", () => {
    const userItems = findElementBtAttrs(wrapper, "user-items");
    expect(userItems).toHaveLength(0);
  });
  it("should update state on click", () => {
    const inputAddBtn = findElementBtAttrs(wrapper, "add-input-element");
    const addBtn = findElementBtAttrs(wrapper, "button-Add-1");
    inputAddBtn.simulate("change", { target: { value: "random" } });
    addBtn.simulate("click");
    const userItems = findElementBtAttrs(wrapper, "user-items");
    expect(userItems).toHaveLength(1);
  });
  it("should update state with multiple user and and check", () => {
    const inputAddBtn = findElementBtAttrs(wrapper, "add-input-element");
    const addBtn = findElementBtAttrs(wrapper, "button-Add-1");
    inputAddBtn.simulate("change", { target: { value: "random" } });
    addBtn.simulate("click");
    inputAddBtn.simulate("change", { target: { value: "random1" } });
    addBtn.simulate("click");
    const userItems = findElementBtAttrs(wrapper, "user-items");
    expect(userItems).toHaveLength(2);
  });
});

describe("edit and depete the user", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("modifiy user name", () => {
    const inputAddBtn = findElementBtAttrs(wrapper, "add-input-element");
    const addBtn = findElementBtAttrs(wrapper, "button-Add-1");
    inputAddBtn.simulate("change", { target: { value: "random" } });
    addBtn.simulate("click");
    inputAddBtn.simulate("change", { target: { value: "random1" } });
    addBtn.simulate("click");
    inputAddBtn.simulate("change", { target: { value: "random2" } });
    addBtn.simulate("click");
    const editBtn = findElementBtAttrs(wrapper, `button-Edit-1`);
    editBtn.simulate("click");
    const inputEdit = findElementBtAttrs(wrapper, "edit-input-element");
    inputEdit.simulate("change", { target: { value: "text" } });
    const updateBtn = findElementBtAttrs(wrapper, `button-Update-1`);
    updateBtn.simulate("click");
    const editedText = findElementBtAttrs(wrapper, "user-account-1");
    expect(editedText.text()).toBe("text");
  });
  it("delete user name", () => {
    const inputAddBtn = findElementBtAttrs(wrapper, "add-input-element");
    const addBtn = findElementBtAttrs(wrapper, "button-Add-1");
    inputAddBtn.simulate("change", { target: { value: "random" } });
    addBtn.simulate("click");
    inputAddBtn.simulate("change", { target: { value: "random1" } });
    addBtn.simulate("click");
    inputAddBtn.simulate("change", { target: { value: "random2" } });
    addBtn.simulate("click");
    const editBtn = findElementBtAttrs(wrapper, `button-Delete-1`);
    editBtn.simulate("click");
    const userItems = findElementBtAttrs(wrapper, "user-items");
    expect(userItems).toHaveLength(2);
  });
});
