import React from "react";
import { shallow } from "enzyme";
import { findElementBtAttrs, checkProps } from "../testUtils";
import Button from "./Button";

const clickHandler = jest.fn();

const defaultProps = {
  id: 1,
  name: "add",
  clickHandler,
  buttonStyle: "addButton",
};

const setup = (props = { ...defaultProps }) => shallow(<Button {...props} />);

describe("renders Button component without fail", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("renders Button component without fail", () => {
    const buttonComp = findElementBtAttrs(wrapper, "button-component");
    expect(buttonComp.exists()).toBeTruthy();
  });
  it("verify for button element is present", () => {
    const button = findElementBtAttrs(
      wrapper,
      `button-${defaultProps.name}-${defaultProps.id}`
    );
    expect(button.exists()).toBeTruthy();
  });
  it("perform click on button and check it have been called", () => {
    const button = findElementBtAttrs(
      wrapper,
      `button-${defaultProps.name}-${defaultProps.id}`
    );
    button.simulate("click");
    expect(clickHandler).toHaveBeenCalled();
  });
});

it("verify proptypes", () => {
  checkProps(Button, defaultProps);
});
