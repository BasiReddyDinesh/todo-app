import PropTypes from "prop-types";
import React, { Component } from "react";

export default class Input extends Component {
  static propTypes = {
    inputStyle: PropTypes.string,
    type: PropTypes.string.isRequired,
    onChangeHandler: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
    placeHolder: PropTypes.string,
  };

  render() {
    return (
      <div data-test="input-component">
        <input
          className={this.props.inputStyle}
          data-test={this.props.id}
          value={this.props.value}
          type={this.props.type}
          onChange={(eve) => this.props.onChangeHandler(eve)}
          placeholder={this.props.placeHolder && this.props.placeHolder}
        ></input>
      </div>
    );
  }
}
