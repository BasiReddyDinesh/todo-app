import PropTypes from "prop-types";
import React, { Component } from "react";

export default class Button extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    clickHandler: PropTypes.func.isRequired,
    buttonStyle: PropTypes.string.isRequired,
  };

  render() {
    return (
      <span data-test="button-component">
        <button
          className={this.props.buttonStyle}
          data-test={`button-${this.props.name}-${this.props.id}`}
          onClick={this.props.clickHandler}
        >
          {this.props.name}
        </button>
      </span>
    );
  }
}
