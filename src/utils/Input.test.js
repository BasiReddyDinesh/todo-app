import { shallow } from "enzyme";
import { findElementBtAttrs, checkProps } from "../testUtils";

import Input from "./Input";

const onChangeHandler = jest.fn();

const defaultProps = {
  type: "text",
  onChangeHandler,
  id: "add-input-element",
};

const setup = (props = { ...defaultProps }) => shallow(<Input {...props} />);

describe("renders Input component without fail", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("renders Input component without fail", () => {
    const inputComp = findElementBtAttrs(wrapper, "input-component");
    expect(inputComp.exists()).toBeTruthy();
  });
  it("veriry for input element is present", () => {
    const input = findElementBtAttrs(wrapper, "add-input-element");
    expect(input.exists()).toBeTruthy();
  });
  it("simpulate onChange and verify it hava been called", () => {
    const input = findElementBtAttrs(wrapper, "add-input-element");
    input.simulate("change", { target: { value: "" } });
    expect(onChangeHandler).toBeCalled();
  });
});

it("verify proptypes", () => {
  checkProps(Input, defaultProps);
});
