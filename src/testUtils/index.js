import checkPropTypes from "check-prop-types";

const findElementBtAttrs = (wrapper, val) => {
  return wrapper.find(`[data-test='${val}']`);
};

const checkProps = (component, props) => {
  const propErr = checkPropTypes(
    component.propTypes,
    props,
    "prop",
    component.name
  );
  expect(propErr).toBeUndefined();
};

export { findElementBtAttrs, checkProps };
